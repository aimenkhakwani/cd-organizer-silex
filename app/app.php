<?php
    date_default_timezone_set("America/Los_Angeles");
    require_once __DIR__."/../vendor/autoload.php";
    require_once __DIR__."/../src/CD.php";

    session_start();

    if (empty($_SESSION['collection'])) {
        $_SESSION['collection'] = array();
    }

    $app = new Silex\Application();

    $app->register(new Silex\Provider\TwigServiceProvider(), array(
        'twig.path' => __DIR__.'/../views'
    ));

    $app->get("/", function() use ($app) {
        return $app['twig']->render('main.html.twig', array('collection' => $_SESSION['collection']));
    });

    $app->get("/add_cd", function() use ($app) {
        return $app['twig']->render('add_cd.html.twig');
    });

    $app->post("/update_collection", function() use ($app) {
        $newCD = new CD($_POST['cd_title'],$_POST['cd_artist'],$_POST['cd_year']);
        array_push($_SESSION['collection'], $newCD);
        return $app['twig']->render('main.html.twig', array('collection' => $_SESSION['collection']));
    });

    $app->get("/search", function() use ($app) {

        $cds_matching_search = array();

        foreach ($_SESSION['collection'] as $cd) {
            if ($cd->matchArtist($_GET['searchString'])) {
                array_push($cds_matching_search, $cd);
            }
        }
        return $app['twig']->render('search_results.html.twig', array('results' => $cds_matching_search, 'searchstring' => $_GET['searchString']));

    });

    return $app;
?>
