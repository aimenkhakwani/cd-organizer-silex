<?php
    class CD
    {
        private $title;
        private $artist;
        private $year;

        function __construct($title, $artist, $year)
        {
            $this->title = $title;
            $this->artist = $artist;
            $this->year = $year;
        }

        function getCDSummary()
        {
            return $this->artist . " '" . $this->title . "' (" . $this->year . ")";
        }

        function matchArtist($searchArtist){
            $regexString = "/" . strtolower($searchArtist) . "/i";
            return preg_match($regexString, $this->artist);
        }
    }

?>
